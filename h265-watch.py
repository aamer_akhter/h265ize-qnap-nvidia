#!/usr/bin/python

import os
import subprocess
import time

def get_files_by_file_size(dirname, reverse=False):
    """ Return list of file paths in directory sorted by file size """

    # Get list of files
    filepaths = []
    for basename in os.listdir(dirname):
        filename = os.path.join(dirname, basename)
        if os.path.isfile(filename):
            filepaths.append(filename)

    # Re-populate list with filename, size tuples
    for i in xrange(len(filepaths)):
        filepaths[i] = (filepaths[i], os.path.getsize(filepaths[i]))

    # Sort list by file size
    # If reverse=True sort from largest to smallest
    # If reverse=False sort from smallest to largest
    filepaths.sort(key=lambda filename: filename[1], reverse=reverse)

    # Re-populate list with just filenames
    for i in xrange(len(filepaths)):
        filepaths[i] = filepaths[i][0]

    return filepaths

def execute(cmd):
    popen = subprocess.Popen(cmd, stdout=subprocess.PIPE, universal_newlines=True)
    for stdout_line in iter(popen.stdout.readline, ""):
        yield stdout_line
    popen.stdout.close()
    return_code = popen.wait()
    if return_code:
        raise subprocess.CalledProcessError(return_code, cmd)

def blacklist_add(filename):
        with open(blacklist,"a") as blfile:
                blfile.write("{}\n".format(filename))

def blacklist_exists(filename):
        with open(blacklist,"r") as blfile:
                if "{}\n".format(filename) in blfile.read():
                        return True
        return False

def is_fail(text,filename):
        if "Processed encode did not meet max time slippage requirements" in text:
                blacklist_add(filename)
                return True
        if "ffmpeg exited with an error" in text:
                blacklist_add(filename)
                return True
        if "Already encoded in h265" in text:
                blacklist_add(filename)
                return True
        if "ffmpeg exited with code 1" in text:
                blacklist_add(filename)
                return True
        if "ffmpeg exited with code 69:" in text:
                blacklist_add(filename)
                return True
        return False

input='/input'
output='/output'
blacklist='/input/blacklist.txt'
files=get_files_by_file_size(input,reverse=True)

for file in files:
        if "blacklist.txt" in file: continue
        if not os.path.exists(file): continue
        if 'ASCII' in subprocess.check_output(["file",file]): continue
        #check to see if file is stable
        fl_size=os.path.getsize(file)
        time.sleep(1)
        if fl_size!=os.path.getsize(file):
                print("file size changed {}, skipping for now".format(file))
                continue
        cmd=['h265ize', file, "-d", "{}".format(output)]
        if blacklist_exists(file):
                print("skipping, blacklist {}".format(file))
                continue
        print(cmd)
        h265=execute(cmd)
        for i in h265:
#               i=i.rstrip()
                is_fail(i,file)
                print(i),

exit(0)
