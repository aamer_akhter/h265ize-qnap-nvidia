FROM amd64/ubuntu:18.04
MAINTAINER aakhter

LABEL architecture="amd64" \
	com.qnap.qcs.gpu="True" \
	com.qnap.qcs.network.mode="nat"

ENV PATH /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
ENV LD_LIBRARY_PATH /lib/x86_64-linux-gnu:/usr/local/nvidia/lib:/usr/local/nvidia/syslib:/usr/local/nvidia/applib:/usr/local/NVIDIA_GPU_DRV/cuda-10.0/lib64/



RUN apt-get update
RUN apt -y install enca dbus npm git
RUN npm install aakhter/h265ize --global --no-optional
RUN ln -s /usr/local/nvidia/bin/ffmpeg /usr/bin/ffmpeg
RUN ln -s /usr/local/nvidia/bin/ffprobe /usr/bin/ffprobe

VOLUME ["/input", "/output", "/usr/local/nvidia","/usr/local/NVIDIA_GPU_DRV"]
WORKDIR /h265ize
COPY h265-watch.py /h265ize/
RUN chmod +x /h265ize/h265-watch.py

ENTRYPOINT ["/h265ize/h265-watch.py"]
